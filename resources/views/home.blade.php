@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
    <button onclick="callEvent()" class="btn btn-primary">Call event</button>
</div>
<script>
		
    Echo.channel('channel1')
      .listen('NewComment', (msg) => {
            console.log(msg.chatMessage);
      })
    
    function callEvent(){
        axios.get('/call-event')
            .then((response) => {
                
            })
            .catch(function (error) {
                console.log(error);
            });
    }  
</script>
@endsection
